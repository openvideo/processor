<?php
use CRM_Civisplitprocessor_ExtensionUtil as E;

class CRM_Civisplitprocessor_BAO_CivisplitProcessorLog extends CRM_Civisplitprocessor_DAO_CivisplitProcessorLog {

  /**
   * Create a new CivisplitProcessorLog based on array-data
   *
   * @param array $params key-value pairs
   * @return CRM_Civisplitprocessor_DAO_CivisplitProcessorLog|NULL
   *
  public static function create($params) {
    $className = 'CRM_Civisplitprocessor_DAO_CivisplitProcessorLog';
    $entityName = 'CivisplitProcessorLog';
    $hook = empty($params['id']) ? 'create' : 'edit';

    CRM_Utils_Hook::pre($hook, $entityName, CRM_Utils_Array::value('id', $params), $params);
    $instance = new $className();
    $instance->copyValues($params);
    $instance->save();
    CRM_Utils_Hook::post($hook, $entityName, $instance->id, $instance);

    return $instance;
  } */

}
