<?php
use CRM_Civisplitprocessor_ExtensionUtil as E;

class CRM_Civisplitprocessor_BAO_CivisplitProcessorSteps extends CRM_Civisplitprocessor_DAO_CivisplitProcessorSteps {

  /**
   * Create a new CivisplitProcessorSteps based on array-data
   *
   * @param array $params key-value pairs
   * @return CRM_Civisplitprocessor_DAO_CivisplitProcessorSteps|NULL
   *
  public static function create($params) {
    $className = 'CRM_Civisplitprocessor_DAO_CivisplitProcessorSteps';
    $entityName = 'CivisplitProcessorSteps';
    $hook = empty($params['id']) ? 'create' : 'edit';

    CRM_Utils_Hook::pre($hook, $entityName, CRM_Utils_Array::value('id', $params), $params);
    $instance = new $className();
    $instance->copyValues($params);
    $instance->save();
    CRM_Utils_Hook::post($hook, $entityName, $instance->id, $instance);

    return $instance;
  } */

}
