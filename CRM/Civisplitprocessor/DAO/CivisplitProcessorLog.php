<?php

/**
 * @package CRM
 * @copyright CiviCRM LLC https://civicrm.org/licensing
 *
 * Generated from civisplitprocessor/xml/schema/CRM/Civisplitprocessor/CivisplitProcessorLog.xml
 * DO NOT EDIT.  Generated by CRM_Core_CodeGen
 * (GenCodeChecksum:68bec17cb8d1540ed812dfeb01dbd35e)
 */
use CRM_Civisplitprocessor_ExtensionUtil as E;

/**
 * Database access object for the CivisplitProcessorLog entity.
 */
class CRM_Civisplitprocessor_DAO_CivisplitProcessorLog extends CRM_Core_DAO {
  const EXT = E::LONG_NAME;
  const TABLE_ADDED = '';

  /**
   * Static instance to hold the table name.
   *
   * @var string
   */
  public static $_tableName = 'civicrm_civisplit_processor_log';

  /**
   * Should CiviCRM log any modifications to this table in the civicrm_log table.
   *
   * @var bool
   */
  public static $_log = TRUE;

  /**
   * Unique CivisplitProcessorLog ID
   *
   * @var int
   */
  public $id;

  /**
   * FK to CivisplitAgreement
   *
   * @var int
   */
  public $agreement_id;

  /**
   * Step ID within agreement
   *
   * @var int
   */
  public $step_id;

  /**
   * FK to Contact
   *
   * @var int
   */
  public $contact_id;

  /**
   * amount of transaction
   *
   * @var float
   */
  public $amount;

  /**
   * When this amount was calculated (when the processor was run)
   *
   * @var timestamp
   */
  public $date_calculated;

  /**
   * When the amount was paid out
   *
   * @var timestamp
   */
  public $date_paid;

  /**
   * FK to contribution table.
   *
   * @var int
   */
  public $contribution_id;

  /**
   * Class constructor.
   */
  public function __construct() {
    $this->__table = 'civicrm_civisplit_processor_log';
    parent::__construct();
  }

  /**
   * Returns localized title of this entity.
   *
   * @param bool $plural
   *   Whether to return the plural version of the title.
   */
  public static function getEntityTitle($plural = FALSE) {
    return $plural ? E::ts('Civisplit Processor Logs') : E::ts('Civisplit Processor Log');
  }

  /**
   * Returns foreign keys and entity references.
   *
   * @return array
   *   [CRM_Core_Reference_Interface]
   */
  public static function getReferenceColumns() {
    if (!isset(Civi::$statics[__CLASS__]['links'])) {
      Civi::$statics[__CLASS__]['links'] = static::createReferenceColumns(__CLASS__);
      Civi::$statics[__CLASS__]['links'][] = new CRM_Core_Reference_Basic(self::getTableName(), 'agreement_id', 'civicrm_civisplit_agreement', 'id');
      Civi::$statics[__CLASS__]['links'][] = new CRM_Core_Reference_Basic(self::getTableName(), 'contact_id', 'civicrm_contact', 'id');
      Civi::$statics[__CLASS__]['links'][] = new CRM_Core_Reference_Basic(self::getTableName(), 'contribution_id', 'civicrm_contribution', 'id');
      CRM_Core_DAO_AllCoreTables::invoke(__CLASS__, 'links_callback', Civi::$statics[__CLASS__]['links']);
    }
    return Civi::$statics[__CLASS__]['links'];
  }

  /**
   * Returns all the column names of this table
   *
   * @return array
   */
  public static function &fields() {
    if (!isset(Civi::$statics[__CLASS__]['fields'])) {
      Civi::$statics[__CLASS__]['fields'] = [
        'id' => [
          'name' => 'id',
          'type' => CRM_Utils_Type::T_INT,
          'description' => E::ts('Unique CivisplitProcessorLog ID'),
          'required' => TRUE,
          'where' => 'civicrm_civisplit_processor_log.id',
          'table_name' => 'civicrm_civisplit_processor_log',
          'entity' => 'CivisplitProcessorLog',
          'bao' => 'CRM_Civisplitprocessor_DAO_CivisplitProcessorLog',
          'localizable' => 0,
          'html' => [
            'type' => 'Number',
          ],
          'readonly' => TRUE,
          'add' => NULL,
        ],
        'agreement_id' => [
          'name' => 'agreement_id',
          'type' => CRM_Utils_Type::T_INT,
          'description' => E::ts('FK to CivisplitAgreement'),
          'where' => 'civicrm_civisplit_processor_log.agreement_id',
          'table_name' => 'civicrm_civisplit_processor_log',
          'entity' => 'CivisplitProcessorLog',
          'bao' => 'CRM_Civisplitprocessor_DAO_CivisplitProcessorLog',
          'localizable' => 0,
          'FKClassName' => 'CRM_Civisplit_DAO_CivisplitAgreement',
          'add' => NULL,
        ],
        'step_id' => [
          'name' => 'step_id',
          'type' => CRM_Utils_Type::T_INT,
          'description' => E::ts('Step ID within agreement'),
          'where' => 'civicrm_civisplit_processor_log.step_id',
          'table_name' => 'civicrm_civisplit_processor_log',
          'entity' => 'CivisplitProcessorLog',
          'bao' => 'CRM_Civisplitprocessor_DAO_CivisplitProcessorLog',
          'localizable' => 0,
          'add' => NULL,
        ],
        'contact_id' => [
          'name' => 'contact_id',
          'type' => CRM_Utils_Type::T_INT,
          'description' => E::ts('FK to Contact'),
          'where' => 'civicrm_civisplit_processor_log.contact_id',
          'table_name' => 'civicrm_civisplit_processor_log',
          'entity' => 'CivisplitProcessorLog',
          'bao' => 'CRM_Civisplitprocessor_DAO_CivisplitProcessorLog',
          'localizable' => 0,
          'FKClassName' => 'CRM_Contact_DAO_Contact',
          'add' => NULL,
        ],
        'amount' => [
          'name' => 'amount',
          'type' => CRM_Utils_Type::T_MONEY,
          'title' => E::ts('Amount to pay'),
          'description' => E::ts('amount of transaction'),
          'required' => TRUE,
          'precision' => [
            20,
            2,
          ],
          'where' => 'civicrm_civisplit_processor_log.amount',
          'table_name' => 'civicrm_civisplit_processor_log',
          'entity' => 'CivisplitProcessorLog',
          'bao' => 'CRM_Civisplitprocessor_DAO_CivisplitProcessorLog',
          'localizable' => 0,
          'add' => NULL,
        ],
        'date_calculated' => [
          'name' => 'date_calculated',
          'type' => CRM_Utils_Type::T_TIMESTAMP,
          'title' => E::ts('Date Calculated'),
          'description' => E::ts('When this amount was calculated (when the processor was run)'),
          'required' => TRUE,
          'where' => 'civicrm_civisplit_processor_log.date_calculated',
          'default' => 'CURRENT_TIMESTAMP',
          'table_name' => 'civicrm_civisplit_processor_log',
          'entity' => 'CivisplitProcessorLog',
          'bao' => 'CRM_Civisplitprocessor_DAO_CivisplitProcessorLog',
          'localizable' => 0,
          'add' => NULL,
        ],
        'date_paid' => [
          'name' => 'date_paid',
          'type' => CRM_Utils_Type::T_TIMESTAMP,
          'title' => E::ts('Date Paid'),
          'description' => E::ts('When the amount was paid out'),
          'required' => FALSE,
          'where' => 'civicrm_civisplit_processor_log.date_paid',
          'table_name' => 'civicrm_civisplit_processor_log',
          'entity' => 'CivisplitProcessorLog',
          'bao' => 'CRM_Civisplitprocessor_DAO_CivisplitProcessorLog',
          'localizable' => 0,
          'add' => NULL,
        ],
        'contribution_id' => [
          'name' => 'contribution_id',
          'type' => CRM_Utils_Type::T_INT,
          'title' => E::ts('Contribution ID'),
          'description' => E::ts('FK to contribution table.'),
          'required' => FALSE,
          'where' => 'civicrm_civisplit_processor_log.contribution_id',
          'table_name' => 'civicrm_civisplit_processor_log',
          'entity' => 'CivisplitProcessorLog',
          'bao' => 'CRM_Civisplitprocessor_DAO_CivisplitProcessorLog',
          'localizable' => 0,
          'FKClassName' => 'CRM_Contribute_DAO_Contribution',
          'html' => [
            'label' => E::ts("Contribution"),
          ],
          'add' => NULL,
        ],
      ];
      CRM_Core_DAO_AllCoreTables::invoke(__CLASS__, 'fields_callback', Civi::$statics[__CLASS__]['fields']);
    }
    return Civi::$statics[__CLASS__]['fields'];
  }

  /**
   * Return a mapping from field-name to the corresponding key (as used in fields()).
   *
   * @return array
   *   Array(string $name => string $uniqueName).
   */
  public static function &fieldKeys() {
    if (!isset(Civi::$statics[__CLASS__]['fieldKeys'])) {
      Civi::$statics[__CLASS__]['fieldKeys'] = array_flip(CRM_Utils_Array::collect('name', self::fields()));
    }
    return Civi::$statics[__CLASS__]['fieldKeys'];
  }

  /**
   * Returns the names of this table
   *
   * @return string
   */
  public static function getTableName() {
    return self::$_tableName;
  }

  /**
   * Returns if this table needs to be logged
   *
   * @return bool
   */
  public function getLog() {
    return self::$_log;
  }

  /**
   * Returns the list of fields that can be imported
   *
   * @param bool $prefix
   *
   * @return array
   */
  public static function &import($prefix = FALSE) {
    $r = CRM_Core_DAO_AllCoreTables::getImports(__CLASS__, 'civisplit_processor_log', $prefix, []);
    return $r;
  }

  /**
   * Returns the list of fields that can be exported
   *
   * @param bool $prefix
   *
   * @return array
   */
  public static function &export($prefix = FALSE) {
    $r = CRM_Core_DAO_AllCoreTables::getExports(__CLASS__, 'civisplit_processor_log', $prefix, []);
    return $r;
  }

  /**
   * Returns the list of indices
   *
   * @param bool $localize
   *
   * @return array
   */
  public static function indices($localize = TRUE) {
    $indices = [];
    return ($localize && !empty($indices)) ? CRM_Core_DAO_AllCoreTables::multilingualize(__CLASS__, $indices) : $indices;
  }

}
