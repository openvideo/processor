<?php
namespace Civi\Api4\Action\CivisplitAgreement;

use Brick\Math\RoundingMode;
use Brick\Money\Money;
use Civi\Api4\CivisplitAgreement;
use Civi\Api4\CivisplitProcessorLog;
use Civi\Api4\CivisplitProcessorSteps;
use Civi\Civisplit\Yaml;

/**
 * Returns a data structure for reporting on an agreement.
 *
 * {
 *    // The following are direct copies of the data stored in the table.
 *    id: int
 *    agreement: JSON string,
 *    name: name,
 *    status_id: int
 *    created_id: contact id
 *    created_date: timestamp
 *    modified_date: timestamp
 *    is_test: bool
 *
 *    // The following are our own.
 *    currentStepNumber: int, 1 being the first step
 *    totalPayouts: money,
 *    lastPayoutDate: ?datestring
 *    steps: [
 *      {
 *        totalPayouts: money,
 *        percentComplete: money|null,
 *        totalDue: money, // cap/fixed
 *        payees: [
 *          {
 *            ... all the normal payee fields, paymentName, paymentAmount, paymentAddress ...
 *            contact_id,
 *            totalPayouts: money,
 *            percentComplete: money|null,
 *            lastPayoutDate: ?datestring
 *          },
 *          ...
 *        ],
 *        dateStarted: ?datestring
 *        dateCompleted: ?datestring
 *        lastPayoutDate: ?datestring
 *      },
 *      ...
 *    ],
 *    contributions: [
 *    ],
 *    logs: [
 *    ]
 *
 * }
 *
 */
class GetReport extends \Civi\Api4\Generic\AbstractAction {

  /**
   * CivisplitAgreement ID
   *
   * @var int
   */
  protected ?int $id = NULL;

  /**
   * CivisplitAgreement hash
   *
   * @var string
   */
  protected ?string $hash = NULL;

  /**
   * CivisplitAgreement name
   *
   * @var string
   */
  protected ?string $name = NULL;

  /**
   * What format to return amounts in?
   *
   *   format          example    example
   *   'string'      'USD 10.00'
   *   'amount'          '10.00'
   *   'minorAmount'       1000
   *   'brick'       (\Brick\Money\Money objects)
   *   'locale'        '$ 10.00'
   *   'fr_FR'         '$ 10.00'
   *
   */
  protected string $moneyFormat = 'string';

  /**
   * Holds the CivisplitAgreement data.
   */
  protected ?array $_agreementData = [];

  public function _run(\Civi\Api4\Generic\Result $result) {

    $parsed = $this->fetchAgreement();

    $this->initialiseResult($result, $parsed);

    // Load and index the logs
    $logs = CivisplitProcessorLog::get(FALSE)
      ->addSelect('step_id', 'contact_id', 'amount', 'date_calculated', 'date_paid', 'contact_id.display_name', 'contribution_id')
      ->addWhere('agreement_id', '=', $this->_agreementData['id'])
      ->addOrderBy('id')
      ->execute();
    $result['logs'] = [];
    // Rename contact_id.display_name to just display_name and convert money to Brick Money.
    foreach ($logs as $row) {
      $row['display_name'] = $row['contact_id.display_name'];
      $row['amount'] = Money::of($row['amount'], $parsed['currency']);
      unset($row['contact_id.display_name']);
      $result['logs'][] = $row;
    }

    // Load the steps' start/complete dates.
    $steps = CivisplitProcessorSteps::get(FALSE)
      ->addWhere('agreement_id', '=', $this->_agreementData['id'])
      ->addOrderBy('step_id')
      ->execute();
    $firstIncompleteStep = NULL;
    foreach ($steps as $step) {
      $stepIdx = $step['step_id']; // 0 based
      $result['steps'][$stepIdx]['dateStarted'] = $step['date_started'] ?? NULL;
      $result['steps'][$stepIdx]['dateCompleted'] = $step['date_completed'] ?? NULL;
      if (empty($step['date_completed']) && $firstIncompleteStep === NULL) {
        // 1 based.
        $firstIncompleteStep = $stepIdx + 1;
      }
    }
    $result['currentStepNumber'] = $firstIncompleteStep;

    // Load the contribution data? There seems no way to link a contribution to a log?
    // Contributions are not created until payout, maybe.
    // $result['contributions'] = \Civi\Api4\Contribution::get(FALSE)->addSuitableWhereFilter()->execute();

    $this->calculateTotals($result, $parsed);

    // Convert Brick Money objects into strings, without currency.
    $this->castAmountsToOutputFormat($result);

  }

  /**
   * Populates $this->_agreementData and returns parsed agreement.
   *
   * @throws API_Exception if agreement not found.
   */
  protected function fetchAgreement() :array {
    // Find the agreement.
    foreach (['id', 'hash', 'name'] as $candidate) {
      if (!empty($this->$candidate)) {
        $this->_agreementData = CivisplitAgreement::get(FALSE)
          ->addWhere($candidate, '=', $this->$candidate)
          ->execute()->first();
        break;
      }
      $candidate = '(none, but one of id, hash, name is required)';
    }
    if (empty($this->_agreementData)) {
      throw new \API_Exception("Agreement not found using given $candidate");
    }
    $parsed = Yaml::parse($this->_agreementData['agreement']);

    return $parsed;
  }
  /**
   * Initialise the result structure like:
   */
  protected function initialiseResult(\Civi\Api4\Generic\Result $result, array $parsed) {
    // Copy the raw data into the report top level keys.
    foreach ($this->_agreementData as $k => $v) {
      $result[$k] = $v;
    }

    $currency = $parsed['currency'];
    $result['totalPayouts'] = Money::zero($currency);
    $result['lastPayoutDate'] = NULL;
    $result['steps'] = [];
    $result['contributions'] = [];
    $result['logs'] = [];

    foreach ($parsed['steps'] as $stepIdx => $stepDefinition) {

      $stepIsFixed = $stepDefinition['type'] === 'fixed';

      $result['steps'][$stepIdx] = [
        'isFixed'         => $stepIsFixed, /* This is duplication for code readability. */
        'type'            => $stepDefinition['type'], /* This is duplication for code readability. */
        'cap'             => $stepDefinition['cap'] ?? NULL, /* This is duplication for code readability. */
        'totalPayouts'    => Money::zero($currency),
        'totalDue'        => Money::zero($currency),
        'percentComplete' => NULL,
        'payees'          => [],
        'dateStarted'     => NULL,
        'dateCompleted'   => NULL,
        'lastPayoutDate'  => NULL,
      ];

      if (!$stepIsFixed && !empty($stepDefinition['cap'])) {
        $result['steps'][$stepIdx]['totalDue'] = $stepDefinition['cap'];
      }

      if (!$stepIsFixed) {
        $denominator = 0;
        foreach ($stepDefinition['payees'] as $payeeIdx => $payee) {
          $denominator += $payee['paymentAmount'];
        }
      }
      foreach ($stepDefinition['payees'] as $payeeIdx => $payee) {

        $result['steps'][$stepIdx]['payees'][$payeeIdx] = $payee + [
          'totalPayouts'    => Money::zero($currency),
          'percentComplete' => ($stepIsFixed || !empty($stepDefinition['cap'])) ? 0 : NULL,
          'totalDue'        => NULL,
          'lastPayoutDate'  => NULL,
        ];
        if ($stepIsFixed) {
          // Due amount is simply the payment amount.
          $result['steps'][$stepIdx]['payees'][$payeeIdx]['totalDue'] = $payee['paymentAmount'];
          // Total all the fixed due amounts into the step's totalDue key.
          $result['steps'][$stepIdx]['totalDue'] = $result['steps'][$stepIdx]['totalDue']->plus($payee['paymentAmount']);
        }
        else {
          // Ratios, percentages: we can calculate the due amount if there's a cap.
          // Note that the processor uses cleverer ways to divide stuff up.
          // Here, for reporting, this will probably be good enough as we only use this to generate a rounded percentage.
          // @see https://work.screen.is/lab/mova/civisplit-processor/-/issues/7
          if (!empty($stepDefinition['cap'])) {
            $result['steps'][$stepIdx]['payees'][$payeeIdx]['totalDue'] =
              $stepDefinition['cap']->multipliedBy($payee['paymentAmount'])->dividedBy($denominator, RoundingMode::DOWN);
          }
        }
      }
    }
  }
  /**
   *
   */
  protected function calculateTotals(\Civi\Api4\Generic\Result $result, array $parsed) {

    // Create logs indexed by step and civicrm contact id.
    $logsIndexed = [];
    foreach ($result['logs'] as $log) {
      $logsIndexed[$log['step_id']][$log['contact_id']][] = $log;
    }

    // Calc totals
    foreach ($result['steps'] as $stepIdx => &$step) {

      foreach ($step['payees'] as $payeeIdx => &$payee) {
        $paymentAddress = $parsed['steps'][$stepIdx]['payees'][$payeeIdx]['paymentAddress'];
        foreach ($logsIndexed[$stepIdx][$paymentAddress] ?? [] as $log) {
          $payee['totalPayouts'] = $payee['totalPayouts']->plus($log['amount']);

          // Calculate the payee percentComplete figure, if we have what we need to do it.
          if (!empty($payee['totalDue'])) {
            $payee['percentComplete'] = number_format(
              $payee['totalPayouts']->getMinorAmount()->toInt() * 100 / $payee['totalDue']->getMinorAmount()->toInt());
          }

          $payee['contact_id'] = $paymentAddress;
          $step['totalPayouts'] = $step['totalPayouts']->plus($log['amount']);
          $result['totalPayouts'] = $result['totalPayouts']->plus($log['amount']);

          $date = $log['date_calculated']; // should that be paid?
          if (empty($payee['lastPayoutDate']) || $date > $payee['lastPayoutDate']) {
            $payee['lastPayoutDate'] = $date;
          }
          if (empty($step['lastPayoutDate']) || $date > $step['lastPayoutDate']) {
            $step['lastPayoutDate'] = $date;
          }
          if (empty($result['lastPayoutDate']) || $date > $result['lastPayoutDate']) {
            $result['lastPayoutDate'] = $date;
          }
        }
        // Remove this data from the report since it could feasibly be an approximation
        // as the processor uses slightly different logic than we have used here.
        unset($payee['totalDue']);
      }

      // Percent complete.
      // - fixed step: paid out ÷ due
      // - capped step: paid out ÷ cap
      // - uncapped, non fixed step: it's Null
      if ($step['isFixed']) {
        $step['percentComplete'] = number_format($step['totalPayouts']->getMinorAmount()->toInt() * 100 / $step['totalDue']->getMinorAmount()->toInt(), 1);
      }
      elseif ($step['cap']) {
        $step['percentComplete'] = number_format($step['totalPayouts']->getMinorAmount()->toInt() * 100 / $step['cap']->getMinorAmount()->toInt(), 1);
      }
    }
  }
  /**
   * Apply the moneyFormat option.
   */
  protected function castAmountsToOutputFormat(\Civi\Api4\Generic\Result $result) {

    switch ($this->moneyFormat) {
    case 'brick':
      // Want to return native Brick\Money\Money objects.
      return;

    case 'amount':
      $convert = function (Money &$var) {
        $var = (string) $var->getAmount();
      };
      break;

    case 'minorAmount':
      $convert = function (Money &$var) {
        $var =  $var->getMinorAmount()->toInt();
      };
      break;

    case 'string':
      $convert = function (Money &$var) {
        $var = (string) $var;
      };
      break;

    case 'locale':
      $locale = \CRM_Core_I18n::getLocale();
      $convert = function (Money &$var) use ($locale) {
        $var = (string) $var->formatTo($locale);
      };
      break;

    default:
      if (preg_match('/^[a-z]{2}_[a-z]{2}$/i', $this->moneyFormat)) {
        $locale = $this->moneyFormat;
        $convert = function (Money &$var) use ($locale) {
          $var = (string) $var->formatTo($locale);
        };
      }
      else {
        throw new \API_Exception("Invalid value '$this->moneyFormat' for moneyFormat.");
      }
    }

    $convert($result['totalPayouts']);

    foreach ($result['steps'] as &$step) {
      $convert($step['totalPayouts']);
      $convert($step['totalDue']);
      if (!empty($step['cap'])) {
        $convert($step['cap']);
      }

      foreach ($step['payees'] as &$payee) {
        $convert($payee['totalPayouts']);
        if ($step['type'] === 'fixed') {
          $convert($payee['paymentAmount']);
        }
      }
    }
    foreach ($result['logs'] as &$row) {
      $convert($row['amount']);
    }

  }
}
