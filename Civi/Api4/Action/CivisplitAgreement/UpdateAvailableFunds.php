<?php
namespace Civi\Api4\Action\CivisplitAgreement;

use Civi\Api4\Generic\Result;
use Civi\Api4\Utils\CoreUtil;
use Civi\API\Exception\UnauthorizedException;
/**
 * @class
 * Fetch balances for agreements and create CivisplitProcessorLog entities if we need to.
 */
class UpdateAvailableFunds extends \Civi\Api4\Generic\AbstractBatchAction {
  use \Civi\Api4\Generic\Traits\DAOActionTrait;
  use \Civi\Api4\Generic\Traits\SelectParamTrait;

  /**
   * Criteria for selecting $ENTITIES to process.
   *
   * @var array
   */
  protected $where = [];

  /**
   * Batch fetch balances function
   */
  public function _run(Result $result) {

    // Insist on only operating on 'agreed' agreements, not draft|completed ones.
    $this->addWhere('status_id:name', '=', 'agreed');
    // Insist on fetching all fields.
    $this->setSelect(['*']);

    $items = $this->getBatchRecords();

    if ($this->getCheckPermissions()) {
      foreach ($items as $key => $item) {
        if (!CoreUtil::checkAccessRecord($this, $item, \CRM_Core_Session::getLoggedInContactID() ?: 0)) {
          throw new UnauthorizedException("ACL check failed");
        }
        $items[$key]['check_permissions'] = TRUE;
      }
    }
    if ($items) {
      $result->exchangeArray($this->fetchFunds($items));
    }
  }

  /**
   * We have to override this because the DAOActionTrait overrides it to limit it to fetching the primary key.
   *
   * @return array
   */
  public function getSelect() {
    return $this->select;
  }
  /**
   * Fetch the funds from each agreement's payment processor.
   */
  function fetchFunds(array $items) {
    $results = [];

    // Load payment processorIDs for these agreements.
    $processorsByAgreementID = \Civi\Api4\CivisplitAgreementPaymentProcessor::get(FALSE)
      ->addWhere('agreement_id', 'IN', array_column($items, 'id'))
      ->execute()->indexBy('agreement_id')->column('payment_processor_id');

    foreach ($items as $agreementData) {
      $agreementID = $agreementData['id'];
      $results[$agreementID] = ['agreement_id' => $agreementID];

      // Fetch the payment processor for this agreement.
      // (Nb. we assume that the ID of a test agreement points to the ID of a test payment processor...)
      $paymentProcessorID = (int) ($processorsByAgreementID[$agreementID] ?? 0);
      if (empty($paymentProcessorID)) {
        $results[$agreementID]['error'] = 'Missing payment processor';
        continue;
      }
      $paymentProcessorObject = \Civi\Payment\System::singleton()->getById($paymentProcessorID);
      if (!method_exists($paymentProcessorObject, 'fetchAccountBalance')) {
        $results[$agreementID]['error'] = "Payment processor (#$paymentProcessorID, " . get_class($paymentProcessorObject) . ") does not support fetching account balances.";
        continue;
      }

      // Use the payment processor to fetch the amount
      $amountAvailable = $paymentProcessorObject->fetchAccountBalance($agreementData);
      // artfulrobot asks: perhaps we only want to do this if the amount is greater than zero?
      \Civi\Civisplit\Event\FundsAvailable::trigger($agreementData['hash'], $amountAvailable);

      $results[$agreementID]['balance'] = $amountAvailable;
    }

    return $results;
  }
}

