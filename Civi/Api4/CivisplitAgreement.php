<?php
namespace Civi\Api4;

use Civi\Api4\Action\CivisplitAgreement\UpdateAvailableFunds;

/**
 * CivisplitAgreement entity.
 *
 * Provided by the CiviSplit Processor extension.
 *
 * @package Civi\Api4
 */
class CivisplitAgreement extends Generic\DAOEntity {

  /**
   * @param bool $checkPermissions
   * @return Civi\Api4\Action\CivisplitAgreement\UpdateAvailableFunds;
   */
  public static function updateAvailableFunds($checkPermissions = TRUE) {
    return (new UpdateAvailableFunds(static::getEntityName(), __FUNCTION__))
      ->setCheckPermissions($checkPermissions);
  }

}
