<?php
namespace Civi\Api4;

/**
 * CivisplitProcessorLog entity.
 *
 * Provided by the CiviSplit Processor extension.
 *
 * @package Civi\Api4
 */
class CivisplitProcessorLog extends Generic\DAOEntity {

}
