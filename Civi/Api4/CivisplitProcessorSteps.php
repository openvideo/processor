<?php
namespace Civi\Api4;

/**
 * CivisplitProcessorSteps entity.
 *
 * Provided by the CiviSplit Processor extension.
 *
 * @package Civi\Api4
 */
class CivisplitProcessorSteps extends Generic\DAOEntity {

}
