<?php
/*
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC. All rights reserved.                        |
 |                                                                    |
 | This work is published under the GNU AGPLv3 license with some      |
 | permitted exceptions and without any warranty. For full license    |
 | and copyright information, see https://civicrm.org/licensing       |
 +--------------------------------------------------------------------+
 */
namespace Civi\Civisplit;

use api\v4\Entity\ParticipantTest;
use Brick\Money\Context\AutoContext;
use Civi\Api4\Log;
use Civi\Civisplit\Exception\AgreementNotProcessableException;
use CRM_Civisplitprocessor_ExtensionUtil as E;
use Brick\Money\Money;
use Civi\Api4\CivisplitAgreement;
use Civi\Civisplit\Event;
use Civi\Civisplit\Exception\AgreementNotFoundException;
use Psr\Log\LogLevel;
// use spec\Http\Mock\Exception;
use \Exception;

class Processor {

  /**
   * @var array
   */
  private $agreement;

  /**
   * @var string
   */
  private $agreementName;

  /**
   * @var Money
   */
  private $amountAvailable;

  /**
   * @var Money
   */
  private $amountToProcess;

  /**
   * The currency of the agreement (eg. 'EUR')
   *
   * @var string
   */
  private $currency;

  /**
   * @var array
   */
  private $steps;

  /**
   * @var int
   */
  private $currentStepID;

  /**
   * @return array
   */
  private function getCurrentStep() {
    return $this->steps[$this->currentStepID];
  }

  /**
   * Get the (database) ID of the agreement
   * @return int|NULL
   */
  public function getAgreementID() {
    return $this->agreement['id'] ?? NULL;
  }

  /**
   * @return string
   */
  private function getCurrency() {
    return $this->currency;
  }

  /**
   * @param string $agreementHash
   * @param string $amountAvailable
   *
   * @throws \API_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   * @throws \Civi\Civisplit\Exception\AgreementNotFoundException
   */
  public function __construct(string $agreementHash, string $amountAvailable) {
    $agreement = CivisplitAgreement::get(FALSE)
      ->addWhere('hash', '=', $agreementHash)
      ->addWhere('is_test', 'IN', [TRUE, FALSE])
      ->execute()
      ->first();
    if (empty($agreement)) {
      throw new AgreementNotFoundException("Agreement not found with Hash: {$agreementHash}");
    }
    $agreement['agreement'] = \Civi\Civisplit\Yaml::parse($agreement['agreement']);
    $this->agreementName = $agreement['name'];
    $this->agreement = $agreement;
    $this->currency = $agreement['agreement']['currency'];
    $this->amountAvailable = Money::of($amountAvailable, $this->getCurrency());
    $this->steps = $agreement['agreement']['steps'];
    $this->agreementStatus = \Civi\Api4\OptionValue::get(FALSE)
      ->addWhere('option_group_id:name', '=', 'civisplit_agreement_status')
      ->addWhere('value', '=', 1)
      ->execute()
      ->first()['name'];
  }

  /**
   * @return Money
   */
  private function calculatePendingPayouts() {
    $pending = \Civi\Api4\CivisplitProcessorLog::get(FALSE)
      ->addSelect('SUM(amount) AS sum')
      ->addWhere('agreement_id', '=', $this->getAgreementID())
      ->addWhere('date_paid', 'IS NULL')
      ->execute()
      ->first();

    return Money::of($pending['sum'] ?? '0.00', $this->getCurrency());
  }

  /**
   * Calculate the amount that should be processed
   *
   * @throws \Brick\Money\Exception\MoneyMismatchException
   * @throws \Brick\Money\Exception\UnknownCurrencyException
   */
  private function calculateAmountToProcess() {
    $pendingPayout = $this->calculatePendingPayouts();
    $this->amountToProcess = $this->amountAvailable->minus($pendingPayout);
    Utils::log(E::SHORT_NAME, LogLevel::DEBUG, "Agreement: {$this->agreementName}: AmountToProcess={$this->amountToProcess}");
  }

  public function process() {
    switch ($this->agreementStatus) {
      case 'agreed':
        break;

      case 'draft':
        throw new AgreementNotProcessableException("Agreement must be approved before it can be processed");

      case 'completed':
      default:
        // @todo Additional statuses to implement
        throw new AgreementNotProcessableException("Cannot process agreement when it is {$this->agreementStatus}");
    }

    $this->calculateAmountToProcess();
    if ($this->amountToProcess->isNegativeOrZero()) {
      Utils::log(E::SHORT_NAME, LogLevel::DEBUG, "Agreement: {$this->agreementName}: No amount to process!");
      return FALSE;
    }
    $this->processSteps();
  }

  private function processSteps() {
    // What step are we processing?
    $stepID = $this->getStepToProcess();

    switch ($this->steps[$stepID]['type']) {
      case 'fixed':
        $isCompletesStep = $this->processStepFixed();
        break;

      case 'percentage':
      case 'ratio':
        $isCompletesStep = $this->processStepRatio($stepID);
        break;

      default:
        Utils::log(E::SHORT_NAME, LogLevel::INFO, "Agreement: {$this->agreementName}: Unknown step type: {$this->steps[$stepID]['type']}");
        break;
    }

    if ($isCompletesStep) {
      // Mark this step completed.
      \Civi\Api4\CivisplitProcessorSteps::update(FALSE)
        ->addWhere('agreement_id', '=', $this->getAgreementID())
        ->addWhere('step_id', '=', $this->currentStepID)
        ->addWhere('date_completed', 'IS NULL')
        ->addValue('date_completed', 'now')
        ->execute();
    }

    if ($this->amountToProcess->isPositive()) {
      // We need to process again because we have a remainder
      // @fixme Could we get into an infinite loop if we always have remainder?
      //   Maybe set min amount to process to avoid that situation.
      $this->processSteps();
    }
  }

  /**
   * Get the step that we should be processing
   *
   * @return int|NULL
   */
  private function getStepToProcess() {
    // Do we have a "current step" that is not completed?
    $step = \Civi\Api4\CivisplitProcessorSteps::get(FALSE)
      ->addWhere('agreement_id', '=', $this->getAgreementID())
      ->addWhere('date_completed', 'IS NULL')
      ->execute()
      ->first();
    if (!empty($step)) {
      // Yes, continue working on that step
      $this->currentStepID = $step['step_id'];
    }
    else {
      // No, do we have a completed step for this agreement?
      $step = \Civi\Api4\CivisplitProcessorSteps::get(FALSE)
        ->addWhere('agreement_id', '=', $this->getAgreementID())
        ->addWhere('date_completed', 'IS NOT NULL')
        ->addOrderBy('date_completed', 'DESC')
        ->execute()
        ->first();
      if (!empty($step)) {
        // Yes, we need to work on the next step
        $currentStepKey = $step['step_id'];
        $stepKeys = array_keys($this->steps);
        foreach ($stepKeys as $stepKey) {
          $nextStepKey = next($stepKeys);
          if ($stepKey == $currentStepKey) {
            break;
          }
        }
        if (empty($nextStepKey)) {
          // @todo work out how to record this
          Utils::log('CivisplitProcessor', LogLevel::DEBUG, 'Next step and no more steps! Setting status=completed');
          \Civi\Api4\CivisplitAgreement::update(FALSE)
            ->addValue('status_id:name', 'completed')
            ->addWhere('id', '=', $this->getAgreementID())
            ->execute();
          throw new Exception('Next step and no more steps! Setting status=completed');
        }
        $this->currentStepID = $nextStepKey;
        // Log that we started processing this step
        \Civi\Api4\CivisplitProcessorSteps::create(FALSE)
          ->addValue('agreement_id', $this->getAgreementID())
          ->addValue('step_id', $this->currentStepID)
          ->execute();
      }
      else {
        // No steps in progress, this is the first time we processed this agreement, start with the first step.
        $this->currentStepID = array_key_first($this->steps);
        // Log that we started processing this step
        \Civi\Api4\CivisplitProcessorSteps::create(FALSE)
          ->addValue('agreement_id', $this->getAgreementID())
          ->addValue('step_id', $this->currentStepID)
          ->execute();
      }
    }
    return $this->currentStepID;
  }

  /**
   * @param int $stepID
   *
   * @return array
   * @throws \API_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  private function getStepData($stepID) {
    $stepData = \Civi\Api4\CivisplitProcessorLog::get()
      ->addSelect('contact_id', 'SUM(amount) AS amountReceived')
      ->addWhere('agreement_id', '=', $this->getAgreementID())
      ->addWhere('step_id', '=', $stepID)
      //->addWhere('date_paid', 'IS NULL')
      ->setGroupBy([
        'contact_id',
      ])
      ->execute()
      ->indexBy('contact_id')
      ->getArrayCopy();
    return $stepData;
  }

  /**
   * @param string $agreementType
   *
   * @return array
   * @throws \API_Exception
   * @throws \Brick\Money\Exception\UnknownCurrencyException
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  private function getPayeeData($agreementType) {
    $stepData = $this->getStepData($this->currentStepID);
    foreach ($this->getCurrentStep()['payees'] as $payee) {
      if (empty($stepData)) {
        $amountReceivedFloat = 0;
      }
      else {
        // do some work
        $amountReceivedFloat = $stepData[$payee['paymentAddress']]['amountReceived'];
      }
      $amountReceived = Money::of($amountReceivedFloat, $this->getCurrency());
      $payeeData[$payee['paymentAddress']] = [
        'amountReceived' => $amountReceived,
      ];
      switch ($agreementType) {
        case 'fixed':
          // The parser will have returned a Brick\Money\Money object
          $payeeData[$payee['paymentAddress']]['amountDue'] = $payee['paymentAmount'];
          break;

        case 'ratio':
          $payeeData[$payee['paymentAddress']]['amountRatio'] = (int) $payee['paymentAmount'];
          break;
      }

    }
    return $payeeData;
  }

  /**
   * Distribute $this->amountToProcess amongst payee(s) in a fixed step.
   *
   * @return bool Whether the step should be considered complete.
   */
  private function processStepFixed() :bool {
    $payeeData = $this->getPayeeData('fixed');

    /** @var array of payeeIndexes that need paying */
    $payeesToPay = [];
    foreach ($payeeData as $payeeIndex => $payee) {
      if ($payee['amountDue']->isGreaterThan($payee['amountReceived'])) {
        $payeesToPay[] = $payeeIndex;
      }
    }
    $amountsToAllocate = $this->amountToProcess->splitWithRemainder(count($payeesToPay));
    /** @var Money $remainder */
    $remainder = end($amountsToAllocate);
    $index = 0;

    $isCompletesStep = TRUE;

    foreach ($payeesToPay as $payeeIndex) {
      $payee = $payeeData[$payeeIndex];

      // How much can we give to this payee?
      $amountForPayee = $amountsToAllocate[$index];
      // Copy vars here *only* for code-completion in phpstorm
      /** @var Money $moneyDue */
      $moneyDue = $payee['amountDue'];
      /** @var Money $moneyReceived */
      $moneyReceived = $payee['amountReceived'];
      /** @var Money $moneyCalculated */
      $moneyCalculated = $moneyReceived->plus($amountForPayee);
      if ($moneyCalculated->isGreaterThanOrEqualTo($moneyDue)) {
        // This payment completes the step for this payee
        // Any "extra" money is added to remainder for the next calculation.
        $remainderForThisCalculation = $moneyCalculated->minus($moneyDue);
        $moneyReceivedForThisCalculation = $amountForPayee->minus($remainderForThisCalculation);
        $remainder = $remainder->plus($remainderForThisCalculation);
        $moneyReceived = $moneyDue;
      }
      else {
        // We have not reached the amount this payee is due for this step - give them all the money they are due.
        $moneyReceived = $moneyReceived->plus($amountForPayee);
        $moneyReceivedForThisCalculation = $amountForPayee;
        $isCompletesStep = FALSE;
      }

      $payee['amountReceived'] = $moneyReceived;
      $payee['amountReceivedForThisCalculation'] = $moneyReceivedForThisCalculation;
      $payeeData[$payeeIndex] = $payee;
      $index++;
    }

    // Now save the calculations
    foreach ($payeesToPay as $payeeIndex) {
      $payee = $payeeData[$payeeIndex];
      \Civi\Api4\CivisplitProcessorLog::create()
        ->addValue('agreement_id', $this->getAgreementID())
        ->addValue('step_id', $this->currentStepID)
        ->addValue('contact_id', $payeeIndex)
        ->addValue('amount', $payee['amountReceivedForThisCalculation']->getAmount()->toFloat())
        ->execute();
    }

    // Update the amount to process. If > 0 we'll run again
    $this->amountToProcess = $remainder;

    return $isCompletesStep;
  }

  /**
   * We split the amount available based on a predefined ratio up to the limit for the step (the "cap").
   *
   * @return bool Whether the step should be considered complete.
   *
   * @throws \API_Exception
   * @throws \Brick\Money\Exception\MoneyMismatchException
   * @throws \Brick\Money\Exception\UnknownCurrencyException
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  private function processStepRatio() :bool {
    $payeeData = $this->getPayeeData('ratio');
    $remainder = Money::of(0, $this->getCurrency());

    $isCompletesStep = FALSE;

    // Handle cap if enabled
    /** @var ?\Brick\Money\Money */
    $cap = $this->getCurrentStep()['cap'] ?? NULL;

    // By default, plan to allocate all the money.
    $amountToAllocate = $this->amountToProcess;

    if ($cap) {
      $amountAllocatedForStep = \Civi\Api4\CivisplitProcessorLog::get(FALSE)
        ->addSelect('SUM(amount) AS sum')
        ->addWhere('agreement_id', '=', $this->getAgreementID())
        ->addWhere('step_id', '=', $this->currentStepID)
        ->execute()
        ->first()['sum'];
      $amountAllocatedForStep = Money::of($amountAllocatedForStep ?? 0, $this->getCurrency());

      if ($amountAllocatedForStep->isGreaterThanOrEqualTo($cap)) {
        // This code path should not run.
        Utils::log('Civisplitprocessor', LogLevel::DEBUG, 'Cap reached');
        return TRUE;
      }
      elseif ($amountAllocatedForStep->plus($this->amountToProcess)->isGreaterThanOrEqualTo($cap)) {
        // We have enough (or more than enough) funds to complete this step.
        $remainder = $amountAllocatedForStep->plus($this->amountToProcess)->minus($cap);
        $amountToAllocate = $cap->minus($amountAllocatedForStep);
        $isCompletesStep = TRUE;
      }
    }

    $allocations = array_column($payeeData, 'amountRatio');
    $amountsToAllocate = $amountToAllocate->allocateWithRemainder(...$allocations);

    /** @var Money $remainder */
    $remainder = $remainder->plus(end($amountsToAllocate));
    // The allocation failed if the amount to allocate all landed back as the remainder.
    $allocationLooksFine = ! $remainder->isEqualTo($amountToAllocate);
    // The allocation of small amounts may fail to accurately reflect ratios...
    $allocationLooksFine &= $this->brickAllocatedCorrectly($allocations, $amountsToAllocate);

    if (!$allocationLooksFine) {
      // There's nothing we can allocate; it's all landed back in the remainder.
      // e.g. GBP 0.01 split 2 ways.
      if ($isCompletesStep) {
        // If there was enough to reach or exceed the cap, but we weren't able to allocate,
        // we have to complete this step now. e.g. cap of GBP 10, split 3 ways is going to leave
        // GBP 0.01.
        return TRUE;
      }
      else {
        // The cap has not been reached, we just need to wait for more funds.
        Utils::log('CivisplitProcessor', LogLevel::DEBUG, "totalAmountReceived=0;Remainder={$this->getCurrency()} {$remainder->getAmount()->toFloat()}");
        $this->amountToProcess = Money::zero($this->getCurrency());
        return FALSE;
      }
    }

    // There is some money to allocate.
    $records = [];
    $payeeIndex = 0;
    foreach ($payeeData as $payeeContactID => &$payee) {
      // How much can we give to this payee?
      $amountForPayee = $amountsToAllocate[$payeeIndex];
      // Add that much to the amount they've received
      $payee['amountReceived'] = $payee['amountReceived']->plus($amountForPayee);
      $records[] = [
        'contact_id' => $payeeContactID,
        'amount' => $amountForPayee->getAmount()->toFloat(), // hmmm.
      ];
      $payeeIndex++;
    }
    unset($payee);

    // Now save the calculations
    \Civi\Api4\CivisplitProcessorLog::save()
      ->setDefaults([
        'agreement_id' => $this->getAgreementID(),
        'step_id' => $this->currentStepID,
      ])
      ->setRecords($records)
      ->execute();

    // Update the amount to process. If > 0 we'll run again
    $this->amountToProcess = $remainder;

    return $isCompletesStep;
  }

  /**
   *
   * Check the ratios held.
   *
   * If ratios are r(n) / ∑r == a(n) / ∑a
   * Then          r(n) × ∑a == a(n) × ∑r
   *
   * And using multiply should rule out any possible rounding errors.
   *
   * @see https://github.com/brick/money/pull/55
   */
  private function brickAllocatedCorrectly(array $ratios, array $amountsToAllocate) :bool {

    // Calc totals.
    $totalRatio = 0;
    $totalAmountAllocated = Money::zero($this->getCurrency());
    foreach ($ratios as $payeeIndex => $ratioN) {
      $totalRatio += $ratioN;
      $totalAmountAllocated = $totalAmountAllocated->plus($amountsToAllocate[$payeeIndex]);
    }

    foreach ($ratios as $payeeIndex => $ratioN) {
      $a = $totalAmountAllocated->multipliedBy($ratioN);
      $b = $amountsToAllocate[$payeeIndex]->multipliedBy($totalRatio);
      if (!$a->isEqualTo($b)) {
        return FALSE;
      }
    }
    return TRUE;
  }


}
