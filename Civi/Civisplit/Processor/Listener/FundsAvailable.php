<?php
/*
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC. All rights reserved.                        |
 |                                                                    |
 | This work is published under the GNU AGPLv3 license with some      |
 | permitted exceptions and without any warranty. For full license    |
 | and copyright information, see https://civicrm.org/licensing       |
 +--------------------------------------------------------------------+
 */
namespace Civi\Civisplit\Processor\Listener;

use Civi\Civisplit\Event;

class FundsAvailable {

  /**
   * Trigger the AgreementProcessor
   *
   * @param \Civi\Civisplit\Event\FundsAvailable $event
   */
  public function onTrigger(Event\FundsAvailable $event) {
    $processor = new \Civi\Civisplit\Processor($event->agreementHash, $event->amountAvailable);
    $processor->process();
  }

}
