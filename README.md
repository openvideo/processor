# civisplitprocessor

This implements the processor for CiviSplit Agreements.

The processor is triggered via the `\Civi\Civisplit\Event\FundsAvailable` event (listen for `civi.civisplit.funds.available`).

It can also be triggered directly via the API3 Civisplit.Fundsavailable for testing.

## Requirements

Brick/Money 0.5.3 - see https://github.com/civicrm/civicrm-core/pull/22246

## Structure

### CivisplitProcessorLog

This entity contains the calculation made each time the processor is run for each contact ID.
It shows the date calculated and the date paid (if applicable).
It can be used to work out exactly what happened at each processor run and also sum together to get totals.

### CivisplitProcessorSteps

This records the start and end date when each step was being processed.

## What is implemented?

### "Fixed" step

The fixed step does not support "cap".

### "Percentage" step

The percentage step must be input as ratios. Eg. each party 25% each would be 1:1:1:1 or 25:25:25:25 if you prefer.

"cap" is implemented and will complete the step when the amount specified in "cap" has been reached.

### Known issues / notes

Looping is not implemented. It is expected that each loop would increment the step IDs so as not to create duplicates in the database and for ease of processing.
Eg. An agreement with two steps would process steps 0, 1 and then next loop steps 2, 3 (which uses the definition from steps 0, 1 respectively).
