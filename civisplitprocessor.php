<?php

require_once 'civisplitprocessor.civix.php';
// phpcs:disable
use CRM_Civisplitprocessor_ExtensionUtil as E;
use Symfony\Component\DependencyInjection\Definition;

// phpcs:enable

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function civisplitprocessor_civicrm_config(&$config) {
  _civisplitprocessor_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_container().
 */
function civisplitprocessor_civicrm_container(\Symfony\Component\DependencyInjection\ContainerBuilder $container) {
  $container->addResource(new \Symfony\Component\Config\Resource\FileResource(__FILE__));
  $container
    ->setDefinition('civi.civisplit.funds.available', new Definition('\Civi\Civisplit\Processor\Listener\FundsAvailable'))
    ->setPublic(TRUE);

  $container->findDefinition('dispatcher')->addMethodCall('addListenerService', ['civi.civisplit.funds.available', ['civi.civisplit.funds.available', 'onTrigger'], 0]);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function civisplitprocessor_civicrm_xmlMenu(&$files) {
  _civisplitprocessor_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function civisplitprocessor_civicrm_install() {
  _civisplitprocessor_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function civisplitprocessor_civicrm_postInstall() {
  _civisplitprocessor_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function civisplitprocessor_civicrm_uninstall() {
  _civisplitprocessor_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function civisplitprocessor_civicrm_enable() {
  _civisplitprocessor_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function civisplitprocessor_civicrm_disable() {
  _civisplitprocessor_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function civisplitprocessor_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _civisplitprocessor_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function civisplitprocessor_civicrm_managed(&$entities) {
  _civisplitprocessor_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function civisplitprocessor_civicrm_angularModules(&$angularModules) {
  _civisplitprocessor_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function civisplitprocessor_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _civisplitprocessor_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function civisplitprocessor_civicrm_entityTypes(&$entityTypes) {
  _civisplitprocessor_civix_civicrm_entityTypes($entityTypes);
}
