<?php

use CRM_Civisplitprocessor_ExtensionUtil as E;
use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;
use Civi\Api4\Contact;
use Civi\Api4\CivisplitAgreement;
use Civi\Civisplit\Yaml;

/**
 * FIXME - Add test description.
 *
 * Tips:
 *  - With HookInterface, you may implement CiviCRM hooks directly in the test class.
 *    Simply create corresponding functions (e.g. "hook_civicrm_post(...)" or similar).
 *  - With TransactionalInterface, any data changes made by setUp() or test****() functions will
 *    rollback automatically -- as long as you don't manipulate schema or truncate tables.
 *    If this test needs to manipulate schema or truncate tables, then either:
 *       a. Do all that using setupHeadless() and Civi\Test.
 *       b. Disable TransactionalInterface, and handle all setup/teardown yourself.
 *
 * @group headless
 */
class Civi_Civisplit_ProcessorTest extends \PHPUnit\Framework\TestCase implements HeadlessInterface, HookInterface, TransactionalInterface {

  public $contacts = [];
  public $agreement;
  public $agreementHash = '';
  /** @var int */
  public $agreementID = 0;

  public function setUpHeadless() {
    // Civi\Test has many helpers, like install(), uninstall(), sql(), and sqlFile().
    // See: https://docs.civicrm.org/dev/en/latest/testing/phpunit/#civitest
    return \Civi\Test::headless()
      ->install('civisplit') /* require civisplit to be installed first */
      ->installMe(__DIR__)
      ->apply();
  }

  public function setUp() {
    parent::setUp();
    $this->agreement = NULL;
    $this->agreementID = 0;
    $this->contacts = [];
  }

  public function tearDown() {
    parent::tearDown();
  }

  /**
   * One fixed step, payee due £100, a single run with an available balance of £100
   *
   * Should log that the contact is due £100 and that should complete the step.
   */
  public function testFixedStepCompletesWhenFullMoneysReceived() {
    $this->createFixture(1, 1);

    $amountAvailable = '100';
    \Civi\Civisplit\Event\FundsAvailable::trigger($this->agreementHash, $amountAvailable);

    // Now we should see some logs for contact 0
    $report = \Civi\Api4\CivisplitAgreement::getReport(FALSE)
      ->setHash($this->agreementHash)
      ->execute();

    // We expect one logged payment
    $this->assertCount(1, $report['logs']);
    $log = $report['logs'][0];
    // Check the correct amount was allocated
    $this->assertLogMatches([
      'step_id'         => 0,
      'contact_id'      => $this->contacts[0],
      'amount'          => 'GBP 100.00',
      'date_calculated' => 'now',
      'date_paid'       => NULL
    ], $report['logs'][0]);

    // This step should be complete now, since it wanted 100 and got 100.
    $this->assertNotNull($report['steps'][0]['dateCompleted'], "This step should be completed now as it is fixed 100 and 100 has been allocated.");
  }
  /**
   * One fixed step, payee due £100, a single run with an available balance of £50.02
   *
   * Should log that the contact is due £50.02, should not complete the step.
   */
  public function testFixedStepNotCompleteWhenPartialMoneysReceived() {
    $this->createFixture(1, 1);

    $amountAvailable = '50.02';
    \Civi\Civisplit\Event\FundsAvailable::trigger($this->agreementHash, $amountAvailable);

    // Now we should see some logs for contact 0
    $report = \Civi\Api4\CivisplitAgreement::getReport(FALSE)
      ->setHash($this->agreementHash)
      ->setMoneyFormat('string')
      ->execute();

    // We expect one logged payment
    $this->assertCount(1, $report['logs']);

    $this->assertLogMatches([
      'step_id'         => 0,
      'contact_id'      => $this->contacts[0],
      'amount'          => 'GBP 50.02',
      'date_calculated' => 'now',
      'date_paid'       => NULL
    ], $report['logs'][0]);

    // This step should be complete now, since it wanted 100 and got 100.
    $this->assertNull($report['steps'][0]['dateCompleted']);

    // Check the log adds up.
    $this->assertEquals('GBP 50.02', (string) $report['totalPayouts']);
    $this->assertDateIsh('now', $report['lastPayoutDate']);
    $this->assertEquals('GBP 50.02', (string) $report['steps'][0]['totalPayouts']);
    $this->assertDateIsh('now', $report['steps'][0]['lastPayoutDate']);
    $this->assertEquals('GBP 50.02', (string) $report['steps'][0]['payees'][0]['totalPayouts']);
    $this->assertDateIsh('now', $report['steps'][0]['payees'][0]['lastPayoutDate']);

  }
  /**
   * One fixed step, payee due £100, a single run with an available balance of £200 received.
   *
   * Note: this might not be ideal behaviour, but we have not discussed what
   * needs to happen in this situation. This test documents what does happen at
   * the mo.
   *
   * @expectedException \Exception
   * @expectedExceptionMessage Next step and no more steps! Setting status=completed
   */
  public function testExceptionThrownWhenTooMuchMoney() {
    $this->createFixture(1, 1);

    $amountAvailable = '200';
    \Civi\Civisplit\Event\FundsAvailable::trigger($this->agreementHash, $amountAvailable);
  }
  /**
   * Fixture 2
   *
   * We check that step 1 is paid out fully when the exact funds are allowed.
   *
   * Then we process with an odd amount that can't be halved and does not complete step 2.
   */
  public function test2FixedSteps() {
    $this->createFixture(2, 3);

    \Civi\Civisplit\Event\FundsAvailable::trigger($this->agreementHash, '150');

    // Now we should see some logs
    $report = \Civi\Api4\CivisplitAgreement::getReport(FALSE)
      ->setHash($this->agreementHash)
      ->setMoneyFormat('amount')
      ->execute();

    // print json_encode($report, JSON_PRETTY_PRINT);
    // Because of the way the algorithm works, there should be three logs.
    $this->assertCount(3, $report['logs']);

    $this->assertLogMatches([
      'step_id' => 0,
      'contact_id' => $this->contacts[0],
      'amount' => 75,
      'date_calculated' => 'now',
      'date_paid' => NULL
    ], $report['logs'][0]);

    $this->assertLogMatches([
      'step_id' => 0,
      'contact_id' => $this->contacts[1],
      'amount' => 50,
      'date_calculated' => 'now',
      'date_paid' => NULL
    ], $report['logs'][1]);

    $this->assertLogMatches([
      'step_id' => 0,
      'contact_id' => $this->contacts[0],
      'amount' => 25,
      'date_calculated' => 'now',
      'date_paid' => NULL
    ], $report['logs'][2]);

    // This step should be complete now, since all payees have been fully paid.
    $this->assertNotNull($report['steps'][0]['dateCompleted'], "Step 1 should be completed.");
    // Step 2 should not have been completed.
    $this->assertNull($report['steps'][1]['dateCompleted'], "Step 2 should not be completed.");

    // Check the log adds up.
    $this->assertEquals('150.00', $report['totalPayouts']);
    $this->assertDateIsh('now', $report['lastPayoutDate']);
    $this->assertEquals('150.00', $report['steps'][0]['totalPayouts']);
    $this->assertDateIsh('now', $report['steps'][0]['lastPayoutDate']);
    $this->assertEquals('100.00', $report['steps'][0]['payees'][0]['totalPayouts']);
    $this->assertDateIsh('now', $report['steps'][0]['payees'][0]['lastPayoutDate']);
    $this->assertEquals('50.00', $report['steps'][0]['payees'][1]['totalPayouts']);
    $this->assertDateIsh('now', $report['steps'][0]['payees'][1]['lastPayoutDate']);


    // Part two...
    // We have 150 to payout in step 2, but only an additional 100.01 received, so a balance of 150 + 100.01 = 250.01
    // We expect Barney to be fully paid, but Wilma to only receive 50.01 and the step to remain incomplete.
    \Civi\Civisplit\Event\FundsAvailable::trigger($this->agreementHash, '250.01');

    $report = \Civi\Api4\CivisplitAgreement::getReport(FALSE)
      ->setHash($this->agreementHash)
      ->setMoneyFormat('amount')
      ->execute();

    $this->assertCount(6, $report['logs']);

    $this->assertLogMatches([
      'step_id' => 1,
      'contact_id' => $this->contacts[0],
      'amount' => 50,
      'date_calculated' => 'now',
      'date_paid' => NULL
    ], $report['logs'][3]);

    $this->assertLogMatches([
      'step_id' => 1,
      'contact_id' => $this->contacts[2],
      'amount' => 50,
      'date_calculated' => 'now',
      'date_paid' => NULL
    ], $report['logs'][4]);

    $this->assertLogMatches([
      'step_id' => 1,
      'contact_id' => $this->contacts[0],
      'amount' => 0.01,
      'date_calculated' => 'now',
      'date_paid' => NULL
    ], $report['logs'][5]);

    // Step 2 should not have been completed.
    $this->assertNull($report['steps'][1]['dateCompleted'], "Step 2 should not be completed.");

    // Check the log adds up.
    $this->assertEquals('250.01', (string) $report['totalPayouts']);
    $this->assertDateIsh('now', $report['lastPayoutDate']);
    $this->assertEquals('100.01', $report['steps'][1]['totalPayouts']);
    $this->assertDateIsh('now', $report['steps'][1]['lastPayoutDate']);
    // Check step 2 and totals
    $this->assertEquals('50.01', $report['steps'][1]['payees'][0]['totalPayouts']);
    $this->assertEquals('50.00', $report['steps'][1]['payees'][1]['totalPayouts']);
    $this->assertDateIsh('now', $report['steps'][1]['payees'][0]['lastPayoutDate']);
    $this->assertDateIsh('now', $report['steps'][1]['payees'][1]['lastPayoutDate']);
    // Just check these are as they were before...
    $this->assertEquals('150.00', $report['steps'][0]['totalPayouts']);
    $this->assertDateIsh('now', $report['steps'][0]['lastPayoutDate']);
    $this->assertEquals('100.00', $report['steps'][0]['payees'][0]['totalPayouts']);
    $this->assertEquals('50.00', $report['steps'][0]['payees'][1]['totalPayouts']);

  }
  /**
   * Fixture 3
   *
   * This is a fixed 10 then 75:25 split on payees.
   */
  public function test3Percentage() {
    $this->createFixture(3, 2);

    \Civi\Civisplit\Event\FundsAvailable::trigger($this->agreementHash, '110');

    $report = \Civi\Api4\CivisplitAgreement::getReport(FALSE)
      ->setHash($this->agreementHash)
      ->execute();

    $this->assertCount(3, $report['logs']);
    $this->assertLogMatches([
      'step_id' => 0,
      'contact_id' => $this->contacts[0],
      'amount' => 'GBP 10.00',
      'date_calculated' => 'now',
      'date_paid' => NULL,
    ], $report['logs'][0]);
    $this->assertLogMatches([
      'step_id' => 1,
      'contact_id' => $this->contacts[0],
      'amount' => 'GBP 75.00',
      'date_calculated' => 'now',
      'date_paid' => NULL,
    ], $report['logs'][1]);
    $this->assertLogMatches([
      'step_id' => 1,
      'contact_id' => $this->contacts[1],
      'amount' => 'GBP 25.00',
      'date_calculated' => 'now',
      'date_paid' => NULL,
    ], $report['logs'][2]);


  }
  /**
   * Fixture 3
   *
   * This is a 75:25 split on payees.
   *
   * We may need to fix this here, or wait on Brick:
   * @see https://github.com/brick/money/pull/55
   * @see https://work.screen.is/lab/mova/civisplit-processor/-/issues/6
   */
  public function testTinyAmount() {
    $this->createFixture(3, 2);

    //
    // Test Part 1: give it an amount that can't be allocated correctly.
    //

    // 10.03 = 10 to pay off the fixed bit, but 3p cannot be divided 75:25 so no allocation should occur.
    \Civi\Civisplit\Event\FundsAvailable::trigger($this->agreementHash, '10.03');

    $report = \Civi\Api4\CivisplitAgreement::getReport(FALSE)
      ->setHash($this->agreementHash)
      ->execute();

    // It should create the payout for the fixed just fine, but should not create more payouts.
    // Because 3p can't be divided 75:25. We don't want any thing paid out.
    $this->assertCount(1, $report['logs']);
    $this->assertLogMatches([
      'step_id' => 0,
      'contact_id' => $this->contacts[0],
      'amount' => 'GBP 10.00',
      'date_calculated' => 'now',
      'date_paid' => NULL,
    ], $report['logs'][0]);

    //
    // Test Part 2: give it an amount that can be allocated correctly.
    //

    // 10.04 = 10 to pay off the fixed bit, and 4p CAN be divided 75:25 so allocation SHOULD occur.
    \Civi\Civisplit\Event\FundsAvailable::trigger($this->agreementHash, '10.04');

    $report = \Civi\Api4\CivisplitAgreement::getReport(FALSE)
      ->setHash($this->agreementHash)
      ->execute();

    // It should create the new payouts because 4p can be divided 75:25.
    $this->assertCount(3, $report['logs']);
    // The fixed payout should not have changed.
    $this->assertLogMatches([
      'step_id' => 0,
      'contact_id' => $this->contacts[0],
      'amount' => 'GBP 10.00',
      'date_calculated' => 'now',
      'date_paid' => NULL,
    ], $report['logs'][0]);

    // We should have the 2 new payouts of 3p, 1p respectively.
    $this->assertLogMatches([
      'step_id' => 1,
      'contact_id' => $this->contacts[0],
      'amount' => 'GBP 0.03',
      'date_calculated' => 'now',
      'date_paid' => NULL,
    ], $report['logs'][1]);
    $this->assertLogMatches([
      'step_id' => 1,
      'contact_id' => $this->contacts[1],
      'amount' => 'GBP 0.01',
      'date_calculated' => 'now',
      'date_paid' => NULL,
    ], $report['logs'][2]);

  }
  /**
   * Uses Fixture 6 which splits 2:1 to the first contact (i.e. 2:3 and 1:3)
   *
   * What we're testing here is that Brick allocates money exactly according to the ratio.
   * Specifically, that it does NOT round the 6.66 to 6.67 but instead produces 3.33 and 6.66
   *
   */
  public function testUndividableRatios() {
    $this->createFixture(6, 2);

    \Civi\Civisplit\Event\FundsAvailable::trigger($this->agreementHash, '10');

    $report = \Civi\Api4\CivisplitAgreement::getReport(FALSE)
      ->setHash($this->agreementHash)
      ->execute();

    $this->assertCount(2, $report['logs']);

    $stepDefaults = [
      'step_id'         => 0,
      'date_calculated' => 'now',
      'date_paid'       => NULL,
    ];
    $this->assertLogMatches([ 'contact_id' => $this->contacts[0], 'amount' => 'GBP 6.66' ] + $stepDefaults, $report['logs'][0]);
    $this->assertLogMatches([ 'contact_id' => $this->contacts[1], 'amount' => 'GBP 3.33'  ] + $stepDefaults, $report['logs'][1]);

  }
  /**
   * Fixture 4
   *
   * This is a fixed 10 then 1:1:3 ratio on payees with a cap of 90, then a 100%
   */
  public function testSensibleRatioWithCap() {
    $this->createFixture(4, 3);

    // 70 balance should pay off the fixed 10, then split the remaining 60 as 10, 10, 30
    \Civi\Civisplit\Event\FundsAvailable::trigger($this->agreementHash, '70');

    $report = \Civi\Api4\CivisplitAgreement::getReport(FALSE)
      ->setHash($this->agreementHash)
      ->execute();

    $this->assertCount(4, $report['logs']);
    $this->assertNotNull($report['steps'][0]['dateCompleted'], "This step should be completed now.");
    $this->assertNull($report['steps'][1]['dateCompleted'], "This step should NOT be completed yet.");
    $this->assertNull($report['steps'][2]['dateCompleted'], "This step should NOT be completed yet.");
    $this->assertLogMatches([
      'step_id'         => 0,
      'contact_id'      => $this->contacts[0],
      'amount'          => 'GBP 10.00',
      'date_calculated' => 'now',
      'date_paid'       => NULL,
    ], $report['logs'][0]);

    $stepDefaults = [
      'step_id'         => 1,
      'contact_id'      => $this->contacts[0],
      'amount'          => 'GBP 10.00',
      'date_calculated' => 'now',
      'date_paid'       => NULL,
    ];
    $this->assertLogMatches($stepDefaults, $report['logs'][1]);
    $this->assertLogMatches(['contact_id' => $this->contacts[1], 'amount' => 'GBP 20.00'] + $stepDefaults, $report['logs'][2]);
    $this->assertLogMatches(['contact_id' => $this->contacts[2], 'amount' => 'GBP 30.00'] + $stepDefaults, $report['logs'][3]);

    // Now a balance of 200 should result in... (new items only:)
    // - 200 - 70 already dealt with = 130
    // - 30 of that 130 required up to cap of 90: should result in 5, 10, 15
    // - Remaining 100 should go to Wilma as the 100% recipient in step 3
    \Civi\Civisplit\Event\FundsAvailable::trigger($this->agreementHash, '200');

    $report = \Civi\Api4\CivisplitAgreement::getReport(FALSE)
      ->setHash($this->agreementHash)
      ->execute();

    $this->assertCount(8, $report['logs']);
    $this->assertNotNull($report['steps'][1]['dateCompleted'], "This step should be completed now.");
    $this->assertNull($report['steps'][2]['dateCompleted'], "This step should NOT be completed yet.");
    $m = array_flip($this->contacts);
    // print "\n"; foreach ($report['logs'] as $i => $log) print "Log $i: contact #" . ($m[$log['contact_id']]) . " £" . $log['amount'] . " step $log[step_id]\n";
    $this->assertLogMatches(['amount' => 'GBP 5.00'] + $stepDefaults, $report['logs'][4]);
    $this->assertLogMatches(['contact_id' => $this->contacts[1], 'amount' => 'GBP 10.00'] + $stepDefaults, $report['logs'][5]);
    $this->assertLogMatches(['contact_id' => $this->contacts[2], 'amount' => 'GBP 15.00'] + $stepDefaults, $report['logs'][6]);
    $this->assertLogMatches(['contact_id' => $this->contacts[0], 'amount' => 'GBP 100.00', 'step_id' => 2] + $stepDefaults, $report['logs'][7]);

  }
  /**
   * Fixture 5
   *
   * This is a 100 cap to be split 3 ways (should leave 0.01) then the rest to one person.
   */
  public function testUndividableRatioWithCapStillCompletesStep() {
    $this->createFixture(5, 4);

    // 123 balance. The step 1 cap is 100, but the nearest dividable amount is 99.99, so this should give 23.01 to the final step.
    \Civi\Civisplit\Event\FundsAvailable::trigger($this->agreementHash, '123');

    $report = \Civi\Api4\CivisplitAgreement::getReport(FALSE)
      ->setHash($this->agreementHash)
      ->execute();

    $this->assertCount(4, $report['logs']);
    $this->assertNotNull($report['steps'][0]['dateCompleted'], "This step should be completed now.");
    $this->assertNull($report['steps'][1]['dateCompleted'], "This step should NOT be completed yet.");

    $stepDefaults = [
      'step_id'         => 0,
      'amount'          => 'GBP 33.33',
      'date_calculated' => 'now',
      'date_paid'       => NULL,
    ];
    $this->assertLogMatches([ 'contact_id' => $this->contacts[0] ] + $stepDefaults, $report['logs'][0]);
    $this->assertLogMatches([ 'contact_id' => $this->contacts[1] ] + $stepDefaults, $report['logs'][1]);
    $this->assertLogMatches([ 'contact_id' => $this->contacts[2] ] + $stepDefaults, $report['logs'][2]);

    $this->assertLogMatches([ 'step_id' => 1, 'contact_id' => $this->contacts[3], 'amount' => 'GBP 23.01' ] + $stepDefaults, $report['logs'][3]);

  }
  /**
   * Fixture 5
   *
   * This is a 100 cap to be split 3 ways (should leave 0.01) then the rest to one person.
   * - first balance is 10 which should not complete the step, then next balance is 100 which should complete the step.
   */
  public function testUndividableRatioWithCapDoesNotCompleteIfCapNotMetThenDoesWhenMoreMoneyComesIn() {
    $this->createFixture(5, 4);

    // 10 balance. The step 1 cap is 100, so this is not exceeded. But 10 is not dividable by 3
    \Civi\Civisplit\Event\FundsAvailable::trigger($this->agreementHash, '10');

    $report = \Civi\Api4\CivisplitAgreement::getReport(FALSE)
      ->setHash($this->agreementHash)
      ->execute();

    $this->assertCount(3, $report['logs']);
    $this->assertNull($report['steps'][0]['dateCompleted'], "This step should NOT be completed yet.");
    $this->assertNull($report['steps'][1]['dateCompleted'], "This step should NOT be completed yet.");

    $stepDefaults = [
      'step_id'         => 0,
      'amount'          => 'GBP 3.33',
      'date_calculated' => 'now',
      'date_paid'       => NULL,
    ];
    $this->assertLogMatches([ 'contact_id' => $this->contacts[0] ] + $stepDefaults, $report['logs'][0]);
    $this->assertLogMatches([ 'contact_id' => $this->contacts[1] ] + $stepDefaults, $report['logs'][1]);
    $this->assertLogMatches([ 'contact_id' => $this->contacts[2] ] + $stepDefaults, $report['logs'][2]);

    // New balance is 100; should complete step.
    \Civi\Civisplit\Event\FundsAvailable::trigger($this->agreementHash, '100');

    $report = \Civi\Api4\CivisplitAgreement::getReport(FALSE)
      ->setHash($this->agreementHash)
      ->execute();

    $this->assertCount(7, $report['logs']);
    $this->assertNotNull($report['steps'][0]['dateCompleted'], "This step should be completed now.");
    $this->assertNull($report['steps'][1]['dateCompleted'], "This step should NOT be completed yet.");

    // Each payee has been paid 3.33 already, totalling 9.99. New balance 100 - 9.99 = 90.01 to allocate
    // should be 30 each, remainder 0.01
    $stepDefaults = [
      'step_id'         => 0,
      'amount'          => 'GBP 30.00',
      'date_calculated' => 'now',
      'date_paid'       => NULL,
    ];
    $this->assertLogMatches([ 'contact_id' => $this->contacts[0] ] + $stepDefaults, $report['logs'][3]);
    $this->assertLogMatches([ 'contact_id' => $this->contacts[1] ] + $stepDefaults, $report['logs'][4]);
    $this->assertLogMatches([ 'contact_id' => $this->contacts[2] ] + $stepDefaults, $report['logs'][5]);

    // And step 2 should now have the remaining penny in it.
    $this->assertLogMatches([ 'step_id' => 1, 'contact_id' => $this->contacts[3], 'amount' => 'GBP 0.01' ] + $stepDefaults, $report['logs'][6]);
  }
  /**
   * A very simple agreement with:
   *
   * - one step, fixed
   * - one payee, due £100
   *
   */
  protected function createFixture(int $fixtureID, int $contactsRequired = 1) {

    $contacts = array_slice([
        ['first_name' => 'Wilma', 'last_name' => 'Flintstone'],
        ['first_name' => 'Barney', 'last_name' => 'Rubble'],
        ['first_name' => 'Betty', 'last_name' => 'Rubble'],
        ['first_name' => 'Pebbles', 'last_name' => 'Flintstone'],
      ], 0, $contactsRequired);

    $this->contacts = Contact::save(FALSE)
      ->setRecords($contacts)
      ->setDefaults(['contact_type' => 'Individual'])
      ->execute()->column('id');

    $this->loadAgreementFixture($fixtureID);
  }

  protected function loadAgreementFixture(int $id) {
    // Load RSL, swap out CONTACT_ID_X for $this->contacts[X]
    $rsl = file_get_contents(__DIR__ . "/fixture$id-rsl.yaml");
    $rsl = preg_replace_callback('/CONTACT_ID_(\d+)/', function($match) {
      $i = (int) $match[1];
      if (!isset($this->contacts[$i])) {
        throw new \Exception("RSL requires $match[0] but we haven't made that contact.");
      }
      return $this->contacts[$i];
    }, $rsl);

    // Parse agreement (serves to check it)
    $this->agreement = Yaml::parse($rsl);

    // Save agreement.
    $this->agreementID = CivisplitAgreement::create(FALSE)
      ->addValue('name', $this->agreement['name'])
      ->addValue('status', 'agreed')
      //->addValue('hash', md5($rsl))
      ->addValue('agreement', $rsl)
      ->execute()->first()['id'];

    // Reload to get the hash.
    $this->agreementHash = CivisplitAgreement::get(FALSE)
       ->addWhere('id', '=', $this->agreementID)
       ->execute()->first()['hash'];
  }
  /**
   * Check that a time is pretty much the same as another time, allowing 2s by default.
   */
  protected function assertDateIsh($expected, $actual, string $message = '', int $ishSeconds = 2) {
    if (is_string($expected)) {
      $expectedTime = strtotime($expected);
    }
    else {
      // may one day need to support DateTime or such.
      throw new \Exception("unexpected");
    }
    if (is_string($actual)) {
      $actualTime = strtotime($actual);
    }
    else {
      throw new \Exception("unexpected");
    }

    $this->assertLessThan($ishSeconds, abs($expectedTime - $actualTime), $message);
  }
  /**
   * DRY code to make assertions on a log record.
   */
  protected function assertLogMatches(array $expectedMatches, array $actualLog, string $message = '') {

    foreach ($expectedMatches as $field => $expectedValue) {
      if ($expectedValue === NULL) {
        $this->assertNull($actualLog[$field], $message);
        return;
      }
      else {
        $this->assertNotNull($actualLog[$field], $message);
      }
      if (substr($field, 0, 5) === 'date_') {
        $this->assertDateIsh($expectedValue, $actualLog[$field], "Field $field: " . $message);
      }
      else {
        $this->assertEquals($expectedValue, $actualLog[$field], "Field $field: " . $message);
      }
    }
  }

}
