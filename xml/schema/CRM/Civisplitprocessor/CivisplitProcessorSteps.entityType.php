<?php
// This file declares a new entity type. For more details, see "hook_civicrm_entityTypes" at:
// https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
return [
  [
    'name' => 'CivisplitProcessorSteps',
    'class' => 'CRM_Civisplitprocessor_DAO_CivisplitProcessorSteps',
    'table' => 'civicrm_civisplit_processor_steps',
  ],
];
